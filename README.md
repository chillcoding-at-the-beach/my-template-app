# My Template App

My Template App is an _Android_ application developed with _Kotlin_ language.
It can be used as a start for a new project.

Created from _Navigation Drawer Activity_, code was refactored to use _Kotlin_ extensions and _Anko_ library.
Plus, a little architecture is introduced to structure navigation in the app.
We distinguish both navigation:
 * "push navigation": a new screen is added to the stack
(ie. a new **Activity** is launched)
 * "root navigation": the content of the screen is changed
 (ie. a **FrameLayout** is placed in the main **Activity** and it is used as a container of **Fragment**)

## Demo
![Screen Demo][]

## Class Diagram
![Android Class Diagram][]

[Screen Demo]:
https://gitlab.com/chillcoding-at-the-beach/my-template-app/raw/master/assets/screenDemo.png

[Android Class Diagram]:
https://gitlab.com/chillcoding-at-the-beach/my-template-app/raw/master/assets/myAppClassDiagram.png
