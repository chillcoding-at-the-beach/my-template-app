package com.chillcoding.mystarapp.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.chillcoding.mystarapp.R
import kotlinx.android.synthetic.main.app_bar_my_main.*

class MySecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_second)

        setSupportActionBar(myToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
