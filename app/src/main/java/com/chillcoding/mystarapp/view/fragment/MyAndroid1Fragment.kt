package com.chillcoding.mystarapp.view.fragment

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chillcoding.mystarapp.R

/**
 * Created by macha on 06/10/2017.
 */
class MyAndroid1Fragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater?.inflate(R.layout.fragment_my_android_1, container, false)
        return view!!
    }
}