package com.chillcoding.mystarapp

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.chillcoding.mystarapp.view.MySecondActivity
import com.chillcoding.mystarapp.view.fragment.MyAndroid1Fragment
import com.chillcoding.mystarapp.view.fragment.MyMainFragment
import kotlinx.android.synthetic.main.activity_my_main.*
import kotlinx.android.synthetic.main.app_bar_my_main.*
import org.jetbrains.anko.*

class MyMainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)

        setSupportActionBar(myToolbar)

        val toggle = ActionBarDrawerToggle(
                this, myDrawerLayout, myToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        myDrawerLayout.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        setFragment(MyMainFragment())
    }

    override fun onBackPressed() {
        if (myDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            myDrawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main_option, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        when (item.itemId) {
            R.id.action_star -> showAlertOfStar()
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun showAlertOfStar() {
        alert(R.string.quote_star) {
            title = getString(R.string.title_star)
            positiveButton(R.string.title_star) { showAlertOfStar() }
            noButton { }
        }.show()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_main -> setFragment(MyMainFragment())
            R.id.nav_android_1 -> setFragment(MyAndroid1Fragment())
            R.id.nav_about -> startActivity<MySecondActivity>()
            else -> return false
        }

        myDrawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun setFragment(fragment: Fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.myContent, fragment)
                .commit()
    }

}
